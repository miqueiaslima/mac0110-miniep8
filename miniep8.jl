using Test

# Pode haver alguns problemas com os argumentos pois usei o símbolo em branco para as copas e ouros,
# sendo que há também o símbolo preenchido

function troca(v, i, j)
  aux = v[i]
  v[i] = v[j]
  v[j] = aux
end

function insercao1(v)
# Essa função faz a ordenação usando somente valor como critério
 tam = length(v)
  for i in 2:tam
   j = i
    while j > 1
     if compareByValue(v[j], v[j - 1])
       troca(v, j, j - 1)
     else
       break
     end
    j = j - 1
   end
  end
 return v
end

function insercao2(v)
# Essa função faz a ordenação usando valor e naipe como  critério
 tam = length(v)
  for i in 2:tam
   j = i
    while j > 1
     if compareByValueAndSuit(v[j], v[j - 1])
       troca(v, j, j - 1)
     else
       break
     end
    j = j - 1
   end
  end
 return v
end


function compareByValue(x, y)
    # criei uma matriz pra comparar apenas o valor das cartas.
    # a função consiste em descobrir a coluna da carta x ...
    # ... e compará-la com a coluna de y.
    # o valor da coluna é inserido num vetor vazio, fiz isso pois ... 
    # ... tive problemas pra declarar variáveis.
    a = ["2♠" "3♠" "4♠" "5♠" "6♠" "7♠" "8♠" "9♠" "10♠" "J♠" "Q♠" "K♠" "A♠";
         "2♢" "3♢" "4♢" "5♢" "6♢" "7♢" "8♢" "9♢" "10♢" "J♢" "Q♢" "K♢" "A♢";
         "2♥" "3♥" "4♥" "5♥" "6♥" "7♥" "8♥" "9♥" "10♥" "J♥" "Q♥" "K♥" "A♥";
         "2♣" "3♣" "4♣" "5♣" "6♣" "7♣" "8♣" "9♣" "10♣" "J♣" "Q♣" "K♣" "A♣"]
    u = []
    for i in 1:size(a)[1]
        for j in 1:size(a)[2]
             if x == a[i, j]  
                push!(u, j)
            end
        end
        for k in 1:size(a)[2]
           if y == a[i, k]
            push!(u, k)
          end
        end
    end
    if u[1] >= u[2]
        return false
    else
        return true
    end
end

function compareByValueAndSuit(x, y)
    # semelhante à função anterior, mas nessa ao invés da matriz usei um
    # vetor que contém todas as cartas
    b = [ "2♢" "3♢" "4♢" "5♢" "6♢" "7♢" "8♢" "9♢" "10♢" "J♢" "Q♢" "K♢" "A♢" "2♠" "3♠" "4♠" "5♠" "6♠" "7♠" "8♠" "9♠" "10♠" "J♠" "Q♠" "K♠" "A♠" "2♥" "3♥" "4♥" "5♥" "6♥" "7♥" "8♥" "9♥" "10♥" "J♥" "Q♥" "K♥" "A♥" "2♣" "3♣" "4♣" "5♣" "6♣" "7♣" "8♣" "9♣" "10♣" "J♣" "Q♣" "K♣" "A♣" ]
    c = []
    for i in 1:length(b)
        if x == b[i]
            push!(c, i)
        end
    end
    for k in 1:length(b)
        if y == b[k]
            push!(c, k)
        end
    end
    if c[1] >= c[2]
        return false
    else
        return true
    end
end

function test()
   @test insercao1(["5♣" "4♣" "3♠"]) == ["3♠" "4♣" "5♣"]
   @test insercao1(["3♠" "3♢" "3♥"]) == ["3♠" "3♢" "3♥"]
   @test insercao2(["5♥" "7♢" "10♥" "10♠"]) == ["7♢" "10♠" "5♥" "10♥" ]
   @test insercao2(["A♢" "A♠" "A♥" "A♣"]) == ["A♢" "A♠" "A♥" "A♣"]
   println("Final dos testes")
end

test()
